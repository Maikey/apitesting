package com.epam.training.test_backend.model;

public class Bet {
	
	private Integer id;
	private String description;
	private String type;
	
	// here comes the bet model
	/*[ 
	   { 
	      "id":15,
	      "description":"Fradi Ute win bet",
	      "type":"Win bet"
	   },
	   { 
	      "id":16,
	      "description":"Fradi Ute goals bet",
	      "type":"All goals bet"
	   }
	]*/
	
	public Bet() {}
	
	public Bet(Integer id, String description, String type) {
		super();
		this.id=id;
		this.description=description;
		this.type=type;
		
	}
	
	
	public Integer GetId()
	{return id; }
	public String GetDescription()
	{return description; }
	public String GetType()
	{return type;}
	
	public void SetId(Integer id)
	{this.id=id; }
	public void SetDescription(String description)
	{this.description=description; }
	public void SetType(String type )
	{this.type=type;}
	
	
}
