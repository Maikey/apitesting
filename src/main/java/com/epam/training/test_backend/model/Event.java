package com.epam.training.test_backend.model;

import java.util.List;

import com.epam.training.test_backend.framework.CreateJSONBody;

//import groovyjarjarantlr.collections.List;

public class Event extends CreateJSONBody {
	
	// here comes the event model
	private Integer id;
	private String title;
	private String type;
	private String accountNumber;
	private List<Integer> start;
	private List<Integer> end;

	
	public Event() {}
	//constractor
	public Event(Integer id, String title, String type, String accountNumber, List<Integer> start,List<Integer> end)
	{
		
		super();
		this.id=id;
		this.title=title;
		this.type=type;
		this.accountNumber=accountNumber;
		this.start=start;
		this.end=end;
		
	}
	
	public Integer GetId()
	{return id;}

	public String GetTitle()
	{return title;}
	
	public String GetType()
	{return type;}
	
	public String GetAccountNumber()
	{return accountNumber;}
	
	public List<Integer> GetStart()
	{return start;}
	
	public List<Integer> GetEnd()
	{return end;}
	
	////////////////////
	
	public void SetId(Integer id)
	{this.id=id;}

	public void SetTitle(String title)
	{this.title=title;}
	
	public void SetType(String type)
	{this.type=type;}
	
	public void SetAccountNumber(String accountNumber)
	{this.accountNumber=accountNumber;}
	
	public void SetStart(List<Integer> start )
	{this.start=start;}
	
	public void SetEnd(List<Integer> end)
	{this.end=end;}
	
	
	
}
