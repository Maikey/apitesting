package com.epam.training.test_backend.tests;

import static org.junit.Assert.assertEquals;

import org.json.JSONException;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;

import com.epam.training.test_backend.endpointactions.*;

import com.epam.training.test_backend.endpointactions.Players;
import com.epam.training.test_backend.framework.BasicTest;
import com.epam.training.test_backend.model.Event;
import com.epam.training.test_backend.model.Player;
import com.epam.training.test_backend.model.Bet;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import io.restassured.response.Response;

public class SportsBettingBackEndTest extends BasicTest {

	@Test
	public void verifyUserRegistrationTest() throws JSONException {
		Response returnedPlayer = Players.getPlayerById(userId, sessionId);
		
		Player player = new Player.Builder(Integer.parseInt(userId))
				.withUserName(userName)
				.withName(TEST_NAME)
				.withAccountNumber(accountNumber)
				.withBalance(BALANCE)
				.withCurrency(HUF_CURRENCY)
				.build();
		
		JSONAssert.assertEquals(player.createJSONBodyWithNulls(), returnedPlayer.asString(), true);
	}
	
	@Test
	public void modifyUserTest() throws JSONException {
		Player player = new Player.Builder(Integer.parseInt(userId))
				.withUserName(userName)
				.withName("modifiedNameHere") // modified
				.withAccountNumber(accountNumber)
				.withBalance(42000) // modified
				.withCurrency(HUF_CURRENCY)
				.build();
		
		// update the player here!
		Players.getUpdatePlayerById(player, sessionId);
		Response returnedPlayer = Players.getPlayerById(userId, sessionId);

		// this is an application DB logic here
		player.setVersion(player.getVersion() + 1);
		JSONAssert.assertEquals(player.createJSONBodyWithNulls(), returnedPlayer.asString(), true);
	}
	
	@Test
	public void homeworkTest() {
		int actualNumberOfBets = 0;
		int actualNumberOfEvents = 0;
		
		Gson gson=new Gson();
		
		// get the event and verify the number of them		
		Response ResponseReturnedEvents =Events.getEvents(sessionId);
		Event [] events = gson.fromJson(ResponseReturnedEvents.asString(),Event[].class);//null;// =new Event();
		actualNumberOfEvents=events.length;
		assertEquals("There must be only one event!", 1, actualNumberOfEvents);
		
		// get the bets of the previous event (by its ID) and verify the number of them
		Response ResponseReturnedBets =Events.getBetsByEventId(Integer.toString(events[0].GetId()), sessionId);
		Bet [] bets = gson.fromJson(ResponseReturnedBets.asString(),Bet[].class);//null;//=new Bet();
		actualNumberOfBets=bets.length;
		assertEquals("There must be two bets!", 2, actualNumberOfBets);
	}
	
}
